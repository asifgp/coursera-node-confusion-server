var passport = require('passport');
var User = require('./models/users');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var jwt = require('jsonwebtoken');
var config  = require('./config');

exports.getToken = function(user){
    return jwt.sign(user,config.secrctKey,{expiresIn:3000});
}

var opt={
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.secrctKey
};
exports.jwtPassport = passport.use(new JwtStrategy(opt,
    (jwt_payload,done)=>{
        console.log("JWT Payload: "+jwt_payload);
        user.findOne({_id: jwt_payload._id},(err,user)=>{
            if(err){
                return done(err,false)
            }
            else if (user) {
                return done(null, user);
            }
            else {
                return done(null, false);
            }
        });
    }));

exports.verifyUser = passport.authenticate('jwt',{session:false});


