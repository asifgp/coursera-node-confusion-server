
const mongoose = require('mongoose');
const bluebird = require('bluebird');

const LeadersSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true
    },
    label: {
        type: String,
        required: true
    },
    designation: {
        type: String,
        required: true
    },
    abbr: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: ""
    },
    featured:{
        type: Boolean,
        required: true,
        default: false
    }
},{
    timestamps: true
});
var leaders = mongoose.model('Leader',LeadersSchema);
module.exports = leaders;

