
const mongoose = require('mongoose');
const bluebird = require('bluebird');

require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const PromotionsSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required: true
    },
    label: {
        type: String,
        required: true,
        default: ""
    },
    price: {
        type: Currency,
        required: true,
        min: 1
    },
    description: {
        type: String
    },
    featured: {
        type: Boolean,
        default: false
    }

},{
    timestamps: true
});

var promotions = mongoose.model('Promotion',PromotionsSchema);
module.exports = promotions;

