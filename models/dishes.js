
const mongoose = require('mongoose');
const bluebird = require('bluebird');

require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const commentSchema = new mongoose.Schema({
    rating: {
        type: Number,
        min: 1,
        max: 5,
        required: true
    },
    comment: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    }
    },{
    timestamps: true
    }
);

const dishSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String
    },
    image: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: false
    },
    label: {
        type: String,
        required: true
    },
    price:{
        type: Currency,
        min:0,
        required: true
    },  
    featured:{
        type: Boolean,
        default: false
    },
    comments: [commentSchema]
},{
    timestamps: true
});

var Dishes = mongoose.model('Dish',dishSchema);
module.exports = Dishes;
//Mapped to Index-2




