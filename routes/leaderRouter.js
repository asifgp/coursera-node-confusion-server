
const express = require('express');
const leaderRouter = express.Router();
const mongoose = require('mongoose');
const Leaders = require('../models/leaders');


//Leaders Router
leaderRouter.route('/')
.get((req,res,next)=>{
    Leaders.find()
    .then((leaders)=>{
        res.status=200;
        res.setHeader('Content-Type','application/json');
        res.json(leaders);
    })
    .catch((err)=>{
        next(err);
    });
})
.post((req,res,next)=>{
    Leaders.create(req.body)
    .then((leader)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(leader);
    })
    .catch((err)=>{
        next(err);
    })
})
.put((req,res,next)=>{
    res.statusCode=404;
    res.end('PUT operation not supported on /leaders');
})
.delete((req,res,next)=>{
    Leaders.remove()
    .then((resp)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    })
    .catch((err)=>{
        next(err);
    })
});


//Leader Router - Param
leaderRouter.route('/:leaderId')
.get((req,res,next)=>{
    Leaders.findById(req.params.leaderId)
    .then((leader)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(leader);
    })
    .catch((err)=>{
        next(err);
    })
})
.post((req,res,next)=>{
    res.statusCode = 404;
    res.end("Request Method is not Supported");
})
.put((req,res,next)=>{
    Leaders.findByIdAndUpdate(req.params.leaderId,{
        $set:req.body
    },
    { new: true},
    (err,res)=>{
    })
    .then((resp)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    })
    .catch((err)=>{
        next(err);
    })
})
.delete((req,res,next)=>{
    Leaders.findByIdAndRemove(req.params.leaderId)
    .then((resp)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    })
    .catch((err)=>{
        next(err);
    })
});

module.exports = leaderRouter;




