
const express = require('express');
const promoRouter = express.Router();
const mongoose = require('mongoose');
const Promotions = require('../models/promotions');


//Promotions Router
promoRouter.route('/')
.get((req,res,next)=>{
    Promotions.find()
    .then((promotions)=>{
        res.status=200;
        res.setHeader('Content-Type','application/json');
        res.json(promotions);
    })
    .catch((err)=>{
        next(err);
    });
})
.post((req,res,next)=>{
    Promotions.create(req.body)
    .then((promotion)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(promotion);
    })
    .catch((err)=>{
        next(err);
    })
})
.put((req,res,next)=>{
    res.statusCode=404;
    res.end('PUT operation not supported on /promotions');
})
.delete((req,res,next)=>{
    Promotions.remove()
    .then((resp)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    })
    .catch((err)=>{
        next(err);
    })
});


//Promotion Router - Param
promoRouter.route('/:promoId')
.get((req,res,next)=>{
    Promotions.findById(req.params.promoId)
    .then((promotion)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(promotion);
    })
    .catch((err)=>{
        next(err);
    })
})
.post((req,res,next)=>{
    res.statusCode = 404;
    res.end("Request Method is not Supported");
})
.put((req,res,next)=>{
    Promotions.findByIdAndUpdate(req.params.promoId,{
        $set:req.body
    },
    { new: true},
    (err,res)=>{
    })
    .then((resp)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    })
    .catch((err)=>{
        next(err);
    })
})
.delete((req,res,next)=>{
    Promotions.findByIdAndRemove(req.params.promoId)
    .then((resp)=>{
        res.statusCode=200;
        res.setHeader('Content-Type','application/json');
        res.json(resp);
    })
    .catch((err)=>{
        next(err);
    })
});

module.exports = promoRouter;

