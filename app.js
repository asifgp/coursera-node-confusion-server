
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var FileStore = require('session-file-store')(session);

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var dishRouter = require('./routes/dishRouter');
var leaderRouter = require('./routes/leaderRouter');
var promoRouter = require('./routes/promoRouter');
var passport = require('passport');
var authenticate = require('./authenticate');
var config = require('./config');

var app = express();

const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const Dishes = require('./models/dishes');
//const url = config.mongoUrl;
const url = 'mongodb://asifgptwo:727_7April@ds247058.mlab.com:47058/asifgpfirst';
const connect = mongoose.connect(url,(err)=>{
  if(err){
    console.log('Connection Unsuccessfull');
    console.log(err);
    
  }else{
    console.log('Connection Successfull');    
  }
});

connect.then((db) => {
  console.log('No Error Occured');
}).catch((err) => {
  console.log('Error Occured');
  console.log(err);
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(session({
  name: 'session-id',
  secret: '12345',
  saveUninitialized: true,
  resave: true,
  store: new FileStore()
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser('12345'));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.all('*',(req,res,next)=>{
  if(req.secure){
    next()
  }else{
    res.redirect(307,'https://'+req.hostname+':'+app.get('secPort')+''+req.url);
    console.log('Requested URL : '+'https://'+req.hostname+':'+app.get('port')+''+req.url);
    console.log('Redirected URL : '+'https://'+req.hostname+':'+app.get('secPort')+''+req.url);
  }
})
app.use('/', indexRouter);
app.use('/', usersRouter);
app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promotions', promoRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
